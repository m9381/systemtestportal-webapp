{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div id="tabTestsequences">
    <div class="row tab-side-bar-row">
        <div class="col-md-12">
            {{ if ne .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr }}
            <div class="alert alert-warning">
                <strong>{{T "Warning" .}}!</strong> {{T "This is an older version of the test sequence" .}}
            </div>
            {{ end }}
                <h4 class="mb-3">{{ .Project.Owner }}/{{ .Project.Name }} : {{ .TestSequence.Name }}</h4>
            {{ with .TestSequenceVersion }}
                <div class="form-group">
                    <label><strong>{{T "Test Sequence Description" .}}</strong></label>
                    <p class="text-muted">
                    {{ with .Description }}
                    {{ . }}
                    {{ else }}
                        No Description
                    {{ end }}
                    </p>
                </div>
                <div class="form-group">
                    <label><strong>{{T "Test Sequence Preconditions" .}}</strong></label>
                    <p class="text-muted">
                    {{ with .Preconditions }}
                    {{ . }}
                    {{ else }}
                        {{T "No Conditions" .}}
                    {{ end }}
                    </p>
                </div>
                <div class="form-group">
                    <label><strong>{{T "Test Cases" .}} ({{ len .Cases }})</strong></label>
                {{ if .Cases }}
                    <table class="non-collapsed-table">
                        {{ range $index, $element := .Cases }}
                        <tr>
                            <td colspan="3"><strong>{{ .Name }}</strong></td>
                        </tr>
                        {{ with index .TestCaseVersions 0 }}
                        {{ range $index, $subelement := .Steps }}
                        <tr>
                            <td valign="top"><strong>{{ $index }}. </strong></td>
                            <td valign="top"  class="w-25">{{T "Action" .}}: </td>
                            <td class="w-100 text-muted">{{ .Action }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td valign="top"  class="w-25">{{T "Expected Result" .}}: </td>
                            <td class="w-100 text-muted">{{ .ExpectedResult }}</td>
                        </tr>
                        <tr class="hide-form">
                            <td></td>
                            <td valign="top" class="w-25">{{T "Actual Result" .}}: </td>
                            <td class="w-100 fill-in-area fill-in-area-two-line"> </td>
                        </tr>
                        <tr class="hide-form">
                            <td></td>
                            <td valign="top" class="w-25">{{T "Notes" .}}: </td>
                            <td class="w-100 fill-in-area fill-in-area-two-line"> </td>
                        </tr>
                        <tr class="hide-form">
                            <td colspan="2"></td>
                            <td>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "successful" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "partly failed" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "failed" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "not evaluated" .}}</span>
                                </label>
                            </td>
                        </tr>
                        {{ end }}
                        {{ end }}
                        <tr>
                            <td colspan="3"><strong>{{T "Summary" .}}</strong></td>
                        </tr>
                        <tr class="hide-form">
                            <td></td>
                            <td valign="top" class="w-25">{{T "Notes" .}}: </td>
                            <td class="w-100 fill-in-area fill-in-area-two-line"> </td>
                        </tr>
                        <tr class="hide-form">
                            <td colspan="2"></td>
                            <td>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "successful" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "partly failed" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "failed" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "not evaluated" .}}</span>
                                </label>
                            </td>
                        </tr>
                        {{ end }}
                        <tr>
                            <td colspan="3"><strong>{{T "Result" .}}</strong></td>
                        </tr>
                    </table>
                {{ else }}
                    <p class="text-muted">{{T "No Test Cases" .}}</p>
                {{ end }}
                </div>
                <div class="form-group">
                    <label><strong>{{T "Applicability" .}}</strong></label>
                {{ if .SequenceInfo.Versions}}
                    <div class="form-group">
                        <table id="versionsVariantTable" class="non-collapsed-table w-100">
                            <tr>
                                <td class="w-25"><strong>{{T "Possible Versions" .}}</strong></td>
                                <td class="w-100"><strong>{{T "Possible Variants" .}}</strong></td>
                            </tr>
                        {{ range .SequenceInfo.Versions }}
                            <tr>
                                <td class="br-4">{{ .Name }}:</td>
                                <td>
                                {{ if .Variants }}
                                {{ range .Variants }}
                                {{ .Name }},
                                {{ end }}
                                {{ else }}
                                    <span class="text-muted">{{T "No applicable versions in this version" .}}.</span>span>
                                {{ end }}
                                </td>
                            </tr>
                        {{ end }}
                        </table>
                        <table class="hide-form mt-4 w-100 non-collapsed-table">
                            <tbody>
                            <tr>
                                <td class="w-25">{{T "Executed Version" .}}:</td>
                                <td class="w-100 fill-in-area"> </td>
                            </tr>
                            <tr>
                                <td class="w-25">{{T "Executed Variant" .}}:</td>
                                <td class="w-100 fill-in-area"> </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                {{ else }}
                    <div class="form-group">
                        <p class="text-muted">{{T "This test sequence is not applicable to any system-under-test variants" .}}</p>
                    </div>
                {{ end }}
                </div>
                <div class="form-group">
                    <label><strong>{{T "Estimated Test Duration" .}}</strong></label>
                    <p class="text-muted">
                    {{ if and (eq .SequenceInfo.DurationHours 0) (eq .SequenceInfo.DurationMin 0) }}
                        No Test Duration
                    {{ else }}
                    {{ if gt .SequenceInfo.DurationHours 0 }}
                    {{ .SequenceInfo.DurationHours }}
                    {{ if eq .SequenceInfo.DurationHours 1 }} Hour {{ else }} Hours {{ end }}
                    {{ end }}
                    {{ if gt .SequenceInfo.DurationMin 0 }}
                    {{ .SequenceInfo.DurationMin }}
                    {{ if eq .SequenceInfo.DurationMin 1 }}
                        Minute
                    {{ else }}
                        Minutes
                    {{ end }}
                    {{ end }}
                    {{ end }}
                    </p>
                </div>
            {{ end }}
            {{ with .TestSequence }}
                <div class="form-group">
                    <label><strong>{{T "Labels" .}}</strong></label>
                    <p>
                    {{ range .Labels }}
                        <span class="badge badge-pill" style="background-color: #{{ .Labelcolor }};">
                        {{ .Labelname}}
                        </span>
                    {{ else }}
                        <span class="text-muted">{{T "No Labels" .}}</span>
                    {{ end }}
                    </p>
                </div>
            {{ end }}
        </div>
    </div>
</div>

<script>
    var checkbox = $("#showFormCheckbox");
    checkbox.addClass("d-inline");
    checkbox.removeClass("d-none");
    $("#showForm").change(function(){
        if(this.checked) {
            $(".hide-form").removeClass("hide-form").addClass("show-form");
        } else {
            $(".show-form").removeClass("show-form").addClass("hide-form");
        }
    });
</script>
{{ end }}