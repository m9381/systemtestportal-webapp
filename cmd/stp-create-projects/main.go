/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/store/dummydata"
)

func main() {
	if err := config.Load(); err != nil {
		panic(err)
	}
	store.InitializeDatabase()

	for _, p := range dummydata.Projects {
		if err := store.GetProjectStore().Add(&p); err != nil {
			panic(err)
		}
	}

	for _, tc := range dummydata.Cases {
		if err := store.GetCaseStore().Add(&tc); err != nil {
			panic(err)
		}
	}

	for _, ts := range dummydata.Sequences {
		if err := store.GetSequenceStore().Add(&ts); err != nil {
			panic(err)
		}
	}
}
