/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package json

import (
	"encoding/json"
	"net/http"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// SequencesGet simply serves all test sequences of a project as json.
func SequencesGet(t handler.TestSequenceLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tcs, err := t.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		ts, err := json.Marshal(tcs)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		_, err = w.Write(ts)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}

// Error messages of the marshaller that encodes the test cases
const (
	errMarshallerInvalidCaseIDsTitle = "Invalid test case ids"
	errMarshallerInvalidCaseIDs      = "The sequence information cannot be calculated"
)

// MarshallerError returns an ErrorMessage describing that an Error occurred
// while encoding the test cases
func MarshallerError() errors.HandlerError {
	return errors.ConstructStd(http.StatusBadRequest, errMarshallerInvalidCaseIDsTitle,
		errMarshallerInvalidCaseIDs, nil).
		WithLog("The client send an invalid string of case ids.").
		WithStackTrace(2).
		Finish()
}

// SequenceInfoGet serves detailed information about a testsequence
// as json.
func SequenceInfoGet(tg handler.TestCaseGetter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		testcaseNames := strings.Split(r.FormValue(httputil.NewTestCases), "/")
		testcaseNames = testcaseNames[0 : len(testcaseNames)-1]
		testcaseIDs := make([]id.TestID, len(testcaseNames))
		for i, tcName := range testcaseNames {
			testcaseIDs[i] = id.NewTestID(c.Project.ID(), tcName, true)
		}
		tsi, err := getSequenceInfo(tg, testcaseIDs...)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		b, er := json.Marshal(tsi)
		if er != nil {
			errors.Handle(MarshallerError(), w, r)
			return
		}

		_, err = w.Write(b)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}

//getSequenceInfo calculates the SUTVersion and Duration from the testcaseIds
func getSequenceInfo(tg handler.TestCaseGetter, ids ...id.TestID) (test.SequenceInfo, error) {
	tc, err := handler.GetTestCases(tg, ids...)
	if err != nil {
		return test.SequenceInfo{}, err
	}
	sutVersions, err := test.CalculateSUTVariants(tc)
	if err != nil {
		return test.SequenceInfo{}, err
	}
	duration := test.CalculateDuration(tc)

	si := test.SequenceInfo{
		Versions:      sutVersions,
		DurationHours: int(duration.Hours()),
		DurationMin:   duration.GetMinuteInHour(),
	}
	return si, nil
}

// SequenceGet serves a single sequence as json.
func SequenceGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Sequence == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).DisplayProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	tsJSON, err := json.Marshal(c.Sequence)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	_, err = w.Write(tsJSON)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
}
