/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package usersession

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

const (
	alreadySignedIn = "We don't know how you got here, but your tried to sign in while you " +
		"are already signed in. If you want to sign in with a different account please sign out first ;)"
	givenCredentialsAreWrong = "The given user doesn't exist or the " +
		"given password doesn't match the username."
)

// signin is the handler for signin requests from the client.
type signin struct {
	session Handler
	auth    Auth
}

// NewSigninHandler creates a new signin handler that handles signin requests from the client.
func NewSigninHandler(handler Handler, auth Auth) http.Handler {
	return &signin{handler, auth}
}

// ServeHTTP Handles sign in requests from the client.
func (l *signin) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := l.signinPossible(r); err != nil {
		errors.Handle(err, w, r)
		return
	}
	identifier := r.FormValue(httputil.Identifier)
	password := r.FormValue(httputil.Password)

	if u, err := l.signin(w, r, identifier, password); err != nil {
		errors.Handle(err, w, r)
	} else {
		w.WriteHeader(http.StatusOK)
		httputil.SetHeaderValue(w, httputil.Dashboard, u.Name)
	}
}

const (
	unableToStartSession = "We were unable to start your session. This is our fault. " +
		"We don't know how this happened and are terribly sorry :|"
	unableToAuthenticate = "We were unable to check your " +
		"credentials. We don't know how this happened and are terribly sorry :|"
)

// signin tries to signin the user with given credentials.
// If its successful the signed in user is returned
// if not an error is returned that can be written to the response.
func (l *signin) signin(w http.ResponseWriter, r *http.Request,
	identifier string, password string) (*user.User, error) {

	u, ok, authErr := l.auth.Validate(identifier, password)

	if ok && authErr != nil {
		return nil, errors.Construct(http.StatusUnauthorized).
			WithBody("This user has been deactivated").
			Finish()
	}
	if authErr != nil {
		return nil, errors.Construct(http.StatusInternalServerError).
			WithBody(unableToAuthenticate).
			WithLogf("Authentication error occurred for user %+v", u).
			WithStackTrace(1).
			WithCause(authErr).
			WithRequestDump(r).
			Finish()
	}

	if !ok {
		return nil, errors.
			Construct(http.StatusUnauthorized).
			WithBody(givenCredentialsAreWrong).
			Finish()
	}

	err := l.session.StartFor(w, r, u)
	if err != nil {
		return nil, errors.Construct(http.StatusInternalServerError).
			WithBody(unableToStartSession).
			WithLogf("Unable to start session for user %+v", u).
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Finish()
	}
	return u, nil
}

// signinPossible checks whether a signin is possible.
// This doesn't include checking the users credentials.
// If signin is not possible it returns an error which can be
// written to the response.
func (l *signin) signinPossible(r *http.Request) error {
	su, err := l.session.GetCurrent(r)
	if err != nil {
		return errors.Construct(http.StatusInternalServerError).
			WithBody(unableToCheckSession).
			WithLog("Unable to get current session!").
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Finish()
	}
	if su != nil {
		return errors.
			ConstructWithStackTrace(
				http.StatusBadRequest,
				"User tried to sign in while there is already a signed in in user!").
			WithBody(alreadySignedIn).
			Finish()
	}
	return nil
}
