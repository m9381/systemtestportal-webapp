/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package duration

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

// Some more common durations extending the constants of default time package.
// To count the number of units in a Duration, divide:
//	second := time.Second
//	fmt.Print(int64(second/time.Millisecond)) // prints 1000
//
// To convert an integer number of units to a Duration, multiply:
//	seconds := 10
//	fmt.Print(time.Duration(seconds)*time.Second) // prints 10s
//
const (
	Day   = 24 * time.Hour
	Week  = 7 * Day
	Month = 30 * Day
	Year  = 365 * Day
)

// Duration is an extended version of time.Duration.
// Provides some more functions for formatting and supports units greater than hours.
type Duration struct {
	time.Duration
}

//NewDuration is a constructor for Duration. You don't have to calculate the correct nanosecond-float-value of the
// Duration at your own, but simply insert it into this constructor.
func NewDuration(hours, minutes, seconds int) Duration {
	return Duration{time.Duration(hours)*time.Hour +
		time.Duration(minutes)*time.Minute +
		time.Duration(seconds)*time.Second}
}

//Add sums up two Durations and return the result.
//Nothing will be changed at the summands
func (d Duration) Add(s Duration) Duration {
	return Duration{d.Duration + s.Duration}
}

//Sub subtracts the duration given as parameter from the duration on which function is called and return the result.
//Nothing will be changed at the given variables
func (d Duration) Sub(s Duration) Duration {
	return Duration{d.Duration - s.Duration}
}

//ConvertToHHMMSS converts the Duration to hours, minutes and seconds.
// Precision is cut to seconds.
// Seconds are less 60, 60 seconds are one minutes
// Minutes are less 60, 60 minutes are one hour
// hours can be more than 24, when duration is longer than one day
func (d Duration) ConvertToHHMMSS() (hours, minutes, seconds int) {
	hours = int(d.Hours())
	minutes = d.GetMinuteInHour()
	seconds = d.GetSecondInMinute()
	return
}

// Days returns the duration as a floating point number of days.
func (d Duration) Days() float64 {
	day := d.Duration / Day
	nsec := d.Duration % Day
	return float64(day) + float64(nsec)/(24*60*60*1e9)
}

// Weeks returns the duration as a floating point number of weeks.
func (d Duration) Weeks() float64 {
	week := d.Duration / Week
	nsec := d.Duration % Week
	return float64(week) + float64(nsec)/(7*24*60*60*1e9)
}

// Months returns the duration as a floating point number of months.
func (d Duration) Months() float64 {
	month := d.Duration / Month
	nsec := d.Duration % Month
	return float64(month) + float64(nsec)/(30*24*60*60*1e9)
}

// Years returns the duration as a floating point number of years.
func (d Duration) Years() float64 {
	year := d.Duration / Year
	nsec := d.Duration % Year
	return float64(year) + float64(nsec)/(365*24*60*60*1e9)
}

//GetMonthInYear returns the months of the partial year.
//This is not the whole duration in months!
func (d Duration) GetMonthInYear() int {
	return int(d.Months()) % 12
}

//GetWeekInYear returns the weeks of the partial year.
//This is not the whole duration in weeks!
func (d Duration) GetWeekInYear() int {
	return (int(d.Days()) % 365) / 7
}

//GetDayInYear returns the days of the partial year.
//This is not the whole duration in days!
func (d Duration) GetDayInYear() int {
	return int(d.Days()) % 365
}

//GetDayInMonth returns the days of the partial month.
//This is not the whole duration in days!
func (d Duration) GetDayInMonth() int {
	return int(d.Days()) % 365 % 30
}

//GetDayInWeek returns the days of the partial week.
//This is not the whole duration in days!
func (d Duration) GetDayInWeek() int {
	return int(d.Days()) % 365 % 30 % 7
}

//GetHourInDay returns the hours of the partial day.
//This is not the whole duration in hours!
func (d Duration) GetHourInDay() int {
	return int(d.Hours()) % 24
}

//GetMinuteInHour returns the minutes of the partial hour.
//This is not the whole duration in minutes!
func (d Duration) GetMinuteInHour() int {
	return int(d.Minutes()) % 60
}

//GetSecondInMinute returns the seconds of the partial minute.
//This is not the whole duration in seconds!
func (d Duration) GetSecondInMinute() int {
	return int(d.Seconds()) % 60
}

// InvalidTestDuration returns an ErrorMessage describing that the test duration is invalid
func InvalidTestDuration() errors.HandlerError {
	return errors.ConstructStd(http.StatusBadRequest,
		"Invalid Test Duration", "The test duration "+
			"is invalid", nil).
		WithLog("Invalid test duration send by client.").
		WithStackTrace(2).
		Finish()
}

// GetDuration gets the test case duration as Duration
// Returns -1 if the time is invalid
func GetDuration(hours, mins int) (Duration, error) {
	if mins < 0 || hours < 0 {
		return Duration{Duration: -1}, InvalidTestDuration()
	}
	return NewDuration(hours, mins, 0), nil
}

func (dur *Duration) UnmarshalJSON(b []byte) error {
	var contents map[string]int
	err := json.Unmarshal(b, &contents)
	if err != nil {
		return err
	}
	*dur = NewDuration(contents["hours"], contents["minutes"], contents["seconds"])
	return nil
}
