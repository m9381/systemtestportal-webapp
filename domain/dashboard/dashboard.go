/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package dashboard

import (
	"sort"
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type Dashboard struct {
	Project           *project.Project
	Cases             []*test.Case
	Versions          []*project.Version
	Variants          []project.Variant
	ProtocolMap       map[id.TestID][]test.CaseExecutionProtocol
	DashboardElements []DashboardElement
}

type DashboardElement struct {
	Versions []*project.Version
	Variant  project.Variant
	Results  []CaseResult
}

type CaseResult struct {
	TestCase  *test.Case
	Results   []test.Result
	Protocols []test.CaseExecutionProtocol
}

func NewDashboard(proj *project.Project, cases []*test.Case, protocolMap map[id.TestID][]test.CaseExecutionProtocol) Dashboard {

	dashboardElements := make([]DashboardElement, 0)
	versions := make([]*project.Version, 0)

	var variants = getAllVariantsOfProject(proj)
	variants = SortVariants(variants)

	for _, version := range proj.Versions {
		versions = append(versions, version)
	}
	SortVersions(versions)

	for _, variant := range variants {
		dashboardElements = append(dashboardElements, NewDashboardElement(cases, versions, variant, protocolMap))
	}
	sortDashboardElements(dashboardElements)

	return Dashboard{
		Project:           proj,
		Cases:             cases,
		Versions:          versions,
		Variants:          variants,
		DashboardElements: dashboardElements,
		ProtocolMap:       protocolMap,
	}
}

func NewDashboardElement(cases []*test.Case, versions []*project.Version, variant project.Variant, protocolMap map[id.TestID][]test.CaseExecutionProtocol) DashboardElement {

	results := GetResults(cases, protocolMap, versions, variant)

	return DashboardElement{
		Versions: versions,
		Variant:  variant,
		Results:  results,
	}
}

// GetCaseResultForVariant returns the latest results for all versions of the protocols
func GetCaseResultForVariant(testCase *test.Case, protocols []test.CaseExecutionProtocol, versions []*project.Version, variant project.Variant) CaseResult {

	results := make([]test.Result, 0)
	var caseProtocols []test.CaseExecutionProtocol

	for _, version := range versions {
		if _, ok := testCase.TestCaseVersions[0].Versions[version.Name]; ok &&
			test.ContainsVariant(testCase.TestCaseVersions[0].Versions[version.Name].Variants, variant) {
			caseProtocols = append(caseProtocols, getLatestTestCaseProtocolForVersion(protocols, version, variant))
			results = append(results, getLatestResult(protocols, version, variant))
		} else {
			results = append(results, test.NotApplicable)
		}
	}

	return CaseResult{
		TestCase:  testCase,
		Results:   results,
		Protocols: caseProtocols,
	}

}

func getLatestResult(protocols []test.CaseExecutionProtocol, version *project.Version, variant project.Variant) test.Result {
	var result test.Result
	var latest time.Time

	for _, vari := range version.Variants {
		if vari == variant {
			for _, protocol := range protocols {
				if protocol.SUTVersion == version.Name && protocol.SUTVariant == variant.Name {
					if protocol.ExecutionDate.After(latest) {
						result = protocol.Result
						latest = protocol.ExecutionDate
					}
				}
			}
		}
	}

	return result
}

func GetResults(cases []*test.Case, protocolMap map[id.TestID][]test.CaseExecutionProtocol, versions []*project.Version, variant project.Variant) []CaseResult {
	results := make([]CaseResult, 0)
	// iterate over cases and append results where applicable
	for _, testCase := range cases {
		results = append(results, GetCaseResultForVariant(testCase, protocolMap[testCase.ID()], versions, variant))
	}
	return results
}

func sortDashboardElements(elements []DashboardElement) {
	sort.Slice(elements, func(i, j int) bool {
		cmp := strings.Compare(elements[i].Variant.Name, elements[j].Variant.Name)
		switch cmp {
		case 0:
			return false
		case 1:
			return false
		case -1:
			return true
		default:
			return false
		}
	})
}

func SortVersions(versions []*project.Version) {
	sort.Slice(versions, func(i, j int) bool {
		cmp := strings.Compare(versions[i].Name, versions[j].Name)
		switch cmp {
		case 0:
			return false
		case 1:
			return false
		case -1:
			return true
		default:
			return false
		}
	})
}

func SortVariants(variants []project.Variant) []project.Variant {
	sort.Slice(variants, func(i, j int) bool {
		cmp := strings.Compare(variants[i].Name, variants[j].Name)
		switch cmp {
		case 0:
			return false
		case 1:
			return false
		case -1:
			return true
		default:
			return false
		}
	})
	return variants
}

func getLatestTestCaseProtocolForVersion(protocols []test.CaseExecutionProtocol, version *project.Version, variant project.Variant) test.CaseExecutionProtocol {
	var latest time.Time
	var retProtocol test.CaseExecutionProtocol

	for _, vari := range version.Variants {
		if vari == variant {
			for _, protocol := range protocols {
				if protocol.SUTVersion == version.Name && protocol.SUTVariant == vari.Name {
					if protocol.ExecutionDate.After(latest) {
						retProtocol = protocol
						latest = protocol.ExecutionDate
					}
				}
			}
		}
	}
	return retProtocol
}

func getAllVariantsOfProject(proj *project.Project) []project.Variant {
	var variants []project.Variant

	for _, version := range proj.Versions {
		for _, variant := range version.Variants {
			if !test.ContainsVariant(variants, variant) {
				variants = append(variants, variant)
			}
		}
	}

	if variants == nil {
		variants = append(variants, project.Variant{Name: "No Variants"})
	}

	return variants
}
