/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package dashboard

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/store/dummydata"
	"testing"
)

func TestNewDashboard_Sequences(t *testing.T) {

	proj, sequences, protocolsmap := generateTestData_Sequences()

	dashboard := NewDashboard_Sequences(&proj, sequences, protocolsmap)

	if dashboard.Project.Name != "DuckDuckGo.com" {
		t.Errorf("Wrong project")
	}

	if sequences[0].Name != "Searching" || sequences[1].Name != "Settings" {
		t.Errorf("Wrong test sequences")
	}

	proj = project.Project{}
	sequences = make([]*test.Sequence, 0)
	protocolsmap = make(map[id.TestID][]test.SequenceExecutionProtocol)

	dashboard = NewDashboard_Sequences(&proj, sequences, protocolsmap)
	if len(dashboard.ProtocolMap) != 0 {
		t.Errorf("Shouldn't load anything")
	}
}

func TestSortFunction(t *testing.T) {

	dashboardElements := generateTestDataForSorting_Sequences()

	sortDashboardElements_Sequences(dashboardElements)

	if dashboardElements[0].Variant.Name != "test" {
		t.Errorf("Sorting of dashboard elements went wrong, expected %v got %v", "test", dashboardElements[0].Variant.Name)
	}
}

func generateTestData_Sequences() (project.Project, []*test.Sequence, map[id.TestID][]test.SequenceExecutionProtocol) {
	//Loads DuckDuckGo project
	proj := dummydata.Projects[0]

	//Loads DuckDuckGo test sequences
	sequences := make([]*test.Sequence, 0)

	for _, s := range dummydata.Sequences {
		if s.Project == proj.ID() {
			i := s
			sequences = append(sequences, &i)
		}
	}

	//Loads protocols for testcases
	protocolsmap := make(map[id.TestID][]test.SequenceExecutionProtocol)

	for _, c := range sequences {
		protlist := make([]test.SequenceExecutionProtocol, 0)
		for _, prot := range dummydata.SequenceProtocols() {
			if c.ID() == prot.TestVersion.TestID {
				protlist = append(protlist, prot)
			}
		}
		protocolsmap[c.ID()] = protlist
	}

	return proj, sequences, protocolsmap
}

func generateTestDataForSorting_Sequences() []DashboardElement_Sequences {
	var dashboardElements []DashboardElement_Sequences

	dashboardElements = append(dashboardElements, DashboardElement_Sequences{Variant: project.Variant{Name: "test2"}})
	dashboardElements = append(dashboardElements, DashboardElement_Sequences{Variant: project.Variant{Name: "test"}})
	dashboardElements = append(dashboardElements, DashboardElement_Sequences{Variant: project.Variant{Name: "test"}})

	return dashboardElements
}
