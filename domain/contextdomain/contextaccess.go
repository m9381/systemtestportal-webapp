/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

//---------------------------------------------------------------------------------------------------------------------|
// In Order to access the SystemSettings from the Context, the context needs to access a global singleton which has the
// up to date SystemSettins.
//---------------------------------------------------------------------------------------------------------------------|

package contextdomain

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/system"
)

var globalSystemSettings *system.SystemSettings

//---------------------------------------------------------------------------------------------------------------------|
// FUNCTIONS
//---------------------------------------------------------------------------------------------------------------------|

// SYSTEM SETTINGS -----------------------------------------------------|

func SetGlobalSystemSettings(settings *system.SystemSettings) {
	globalSystemSettings = settings
}

func GetGlobalSystemSettings() *system.SystemSettings {

	// If the globalSystemSettings are accessed by tests, the instance will be nil. So create an instance
	if globalSystemSettings == nil {
		globalSystemSettings = &system.SystemSettings{}
	}

	return globalSystemSettings
}
